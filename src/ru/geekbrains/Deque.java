package ru.geekbrains;

import java.util.EmptyStackException;

public class Deque {
    private int size;
    private int head;
    private int tail;
    private char[] data;
    private int elementCount = 0;

    Deque(int size) {
        data = new char[this.size = size];
    }

    public void pushBack(char value) {
        if (isFull()) throw new StackOverflowError();
        tail++;
        elementCount++;
        if (tail == size) tail = 0;
        data[tail] = value;
    }

    public void pushFront(char value) {
        if (isFull()) throw new StackOverflowError();
        data[head] = value;
        head--;
        elementCount++;
        if (head < 0) head = size - 1;
    }

    public char popBack() {
        if (isEmpty()) throw new EmptyStackException();
        char ret = data[tail];
        tail--;
        elementCount--;
        if (tail < 0) tail = size - 1;
        return ret;
    }

    public char popFront() {
        if (isEmpty()) throw new EmptyStackException();
        head++;
        elementCount--;
        if (head == size) head = 0;
        return data[head];
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return elementCount == 0;
    }

    public boolean isFull() {
        return elementCount == size;
    }
}