package ru.geekbrains;

import java.security.InvalidParameterException;
import java.util.Stack;

public class Main {
    /**
     * Переменная для задачи №3
     */
    private static char[] brackets = {'(', ')', '{', '}', '[', ']'};

    /**
     * Выполнил только два задания порекомендованные вами в ходе занятия.
     */
    public static void main(String[] args) {
        // Задача 3
        System.out.println(checkBrackets("([{1*sin(5)}+4])"));
        System.out.println(checkBrackets("{5+5}([55])+2/4/"));
        System.out.println(checkBrackets("}{{{"));
        // Задача 6
        Deque deque = new Deque(6);
        // Создаем список a b c d e f
        deque.pushFront('b');
        deque.pushFront('a');
        deque.pushBack('c');
        deque.pushBack('d');
        deque.pushBack('e');
        deque.pushBack('f');
        // Извлекаем по очереди элементы с конца и начала списка, должны получить a f b e c d
        for (int i = 3; i < deque.getSize(); i++) {
            System.out.print(" " + deque.popFront());
            System.out.print(" " + deque.popBack());
        }
        System.out.println();
    }

    /**
     * Задача 3
     * Написать программу, которая определяет, является ли введенная скобочная последовательность правильной.
     * Примеры правильных скобочных выражений: (), ([])(), {}(), ([{}]),
     * неправильных — )(, ())({), (, ])}), ([(])
     * для скобок [,(,{.
     *
     * ПРИМЕЧАНИЕ!
     * Реализованный мною алгоритм проверяет правильность расстановки скобок в выражении, но никак не учитывает
     * правильность самого выражения так как этого не требует условие задачи. Это означает, следующие скобочные
     * выражения {}([55])+2/4/, +2//4\t(e)st и тому подобные будут верны. Если задача подразумевала проверку
     * всего выражения на корректность прошу сообщить мне об этом в комментариях к ДЗ, доработаю.
     */
    public static boolean checkBrackets(String expression) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < expression.length(); i++) {
            char symbol = expression.charAt(i);
            if (!isBracket(symbol)) continue;
            if (isCloseBracket(symbol)) {
                if (stack.isEmpty()) return false;
                if (getCloseBracket(stack.pop()) != symbol) return false;
                continue;
            }
            stack.push(symbol);
        }
        return stack.isEmpty();
    }

    /**
     * Проверяет является ли переданный методу символ открывающейся или закрывающейся скобкой.
     *
     * @see brackets
     * @param bracket
     * @return True если переданный методу символ является скобкой
     */
    private static boolean isBracket(char bracket) {
        for (char b : brackets) {
            if (b == bracket) return true;
        }
        return false;
    }

    /**
     * Проверяет является ли переданный методу символ закрывающейся скобкой.
     *
     * @param bracket
     * @return
     */
    private static boolean isCloseBracket(char bracket) {
        for (int i = 0; i < brackets.length; i++) {
            if (brackets[i] == bracket) return i % 2 != 0; // не четные являются закрывающимися скобками
        }
        return false;
    }

    /**
     * Возвращает закрывающуюся скобку для открывающейся скобки переданной в качестве параметра методу.
     *
     * @param openBracket
     * @return
     */
    private static char getCloseBracket(char openBracket) {
        for (int i = 0; i < brackets.length; i++) {
            if (brackets[i] == openBracket) return brackets[i + 1];
        }
        throw new InvalidParameterException();
    }
}
